package senser;

import java.util.ArrayList;

public class AircraftSentenceFactory
{
	public ArrayList<AircraftSentence> fromAircraftJson(String jsonAircraftString)
	{	
		//TODO: Get distinct aircrafts from the jsonAircraftString
				// and store them in an ArrayList
				
				ArrayList<AircraftSentence> finallist = new ArrayList<AircraftSentence>();
				//AircraftSentence dummy = new AircraftSentence(jsonAircraftString);
				
				jsonAircraftString = jsonAircraftString.substring(1);
				
				
				String[] parts = jsonAircraftString.split("\\},\\{");
				
				for(int i = 0; i < parts.length; i++) {
					AircraftSentence AircraftPart = new AircraftSentence(parts[i]);
					finallist.add(AircraftPart);
				}
				
				return finallist;
	}
}
