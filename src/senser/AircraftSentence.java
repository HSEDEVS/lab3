package senser;

public class AircraftSentence
{
	//TODO: Create attributes
			String aircraftData;
		//TODO: Create constructor
			AircraftSentence(String aircraftJson) {
				this.aircraftData = aircraftJson;
			}
			
			AircraftSentence(AircraftSentence data){
				this.aircraftData = data.aircraftData;
			}
			
		//TODO: Create relevant getter methods
			public String getAircraftData() {
				return aircraftData;
			}	
			
		//TODO: Overwrite toString() method to print sentence
			public String toString() {
				return aircraftData;
			}	
}
