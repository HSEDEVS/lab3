package messer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;



public class BasicAircraft {
	private String icao;
	private String operator;
	private Integer species;
	private Date posTime;
	private Coordinate coordinate;
	private Double speed;
	private Double trak;
	private Integer altitude;
	
	//TODO: Create constructor
	BasicAircraft(String icao, String operator, Integer species, Date posTime, Coordinate coordinate, Double speed, Double trak , Integer altitude){
		this.icao = icao;
		this.operator = operator;
		this.species = species;
		this.posTime = posTime;
		this.coordinate = coordinate;
		this.speed = speed;
		this.trak = trak;
		this.altitude = altitude;
		
	}
		
	//TODO: Create relevant getter methods

	public String getIcao() {
		return this.icao;
	}

	public String getOperator() {
		return this.operator;
	}

	public Integer getSpecies() {
		return this.species;
	}

	public Date getPosTime() {
		return this.posTime;
	}

	public Coordinate getCoordinate() {
		return this.coordinate;
	}

	public Double getSpeed() {
		return this.speed;
	}

	public Double getTrak() {
		return this.trak;
	}

	public Integer getAltitude() {
		return this.altitude;
	}
	

	//TODO: Lab 4-6 return attribute names and values for table
	public static ArrayList<String> getAttributesNames()
	{
		return null;
	}

	public static ArrayList<Object> getAttributesValues(BasicAircraft ac)
	{
		return null;
	}

	//TODO: Overwrite toString() method to print fields

	public String toString() {
		return "BasicAircraft [icao=" + icao + ", operator=" + operator + ", species=" + species + ", posTime="
				+ posTime + ", coordinate=" + coordinate + ", speed=" + speed + ", trak=" + trak + ", altitude="
				+ altitude + "]";
	}
}
